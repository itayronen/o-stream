"use strict";
let gulp = require("gulp");
let gulpIgnore = require("gulp-ignore");
let ts = require('gulp-typescript');
let del = require("del");
let merge = require("merge2");

// Config

let codeSource = "src/";
let tsconfigPath = codeSource + 'tsconfig.json';
let typescriptsGlob = [codeSource + "**/*.ts", "!**/*.d.ts"];
let testsDtsGlob = "*.test.d.ts";
let outputDir = "./lib/";

// Tasks

gulp.task("clean", function () { return del([outputDir + '**/*']); });

gulp.task("build", gulp.series("clean", function () {
	let tsProject = ts.createProject(tsconfigPath, { declaration: true });

	let tsResult = gulp.src(typescriptsGlob, { base: codeSource })
		.pipe(tsProject());

	return merge([
		tsResult.js.pipe(gulp.dest(outputDir)),
		tsResult.dts
			.pipe(gulpIgnore.exclude(testsDtsGlob))
			.pipe(gulp.dest(outputDir))
	]);
}));