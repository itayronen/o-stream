import { EnteredArgs, EnteredAsyncArgs } from "./EnteredArgs";
import { EndedArgs, EndedAsyncArgs } from "./EndedArgs";
import { SourceStreamErrorArgs } from "./SourceStreamErrorArgs";
import { TransformArgs, OnEnteredFunc } from "./TransformArgs";
import { Transform } from "stream";

type EnteredAsyncFunc<TIn, TOut> = (args: EnteredAsyncArgs<TIn, TOut>) => void;
type EndedFunc<TIn, TOut> = (args: EndedAsyncArgs<TOut>) => void;
type onSourceStreamError = (args: SourceStreamErrorArgs) => void;

export class ObjectTransform<TIn, TOut> extends Transform {

	private enteredAsync: EnteredAsyncFunc<TIn, TOut>;
	private endedAsync: EndedFunc<TIn, TOut>;
	private onSourceStreamError: onSourceStreamError;

	constructor(args: TransformArgs<TIn, TOut>) {
		super({ objectMode: true });

		args = args || {};

		this.enteredAsync = this.createEnteredAsync(args);
		this.endedAsync = this.createEndedAsync(args);
		this.onSourceStreamError = this.createOnSourceStreamError(args);
		this.listenToSourceStreamErrors();
	}

	public _transform(chunk: TIn, encoding: string, callback: (error?: any) => void): void {
		this.enteredAsync(new EnteredAsyncArgs(chunk, this, callback));
	}

	public _flush(callback: (error?: any) => void): void {
		this.endedAsync(new EndedAsyncArgs(this, callback));
	}

	private createOnSourceStreamError(args: TransformArgs<TIn, TOut>): onSourceStreamError {
		return args.onSourceStreamError ||
			((e: SourceStreamErrorArgs) => { e.emitError(e.error); });
	}

	private listenToSourceStreamErrors(): void {
		let errorListener = (error: any) => {
			let args: SourceStreamErrorArgs = {
				error: error,
				emitError: (error: any) => { this.emit("error", error); }
			};

			try { this.onSourceStreamError(args); }
			catch (error) { this.emit("error", error); }
		};

		this.on("pipe", src => {
			src.on("error", errorListener);
		});

		this.on("unpipe", src => {
			src.removeListener("error", errorListener);
		});
	}

	private createEnteredAsync(params: TransformArgs<TIn, TOut>): EnteredAsyncFunc<TIn, TOut> {
		if (params.onEntered && params.onEnteredAsync) {
			throw new Error("only one of the 'entered' methods can be asigned.");
		}

		return params.onEnteredAsync ||
			(params.onEntered && this.getOnEnteredAsAsync(params.onEntered)) ||
			this.passThroughAsync;
	}

	private createEndedAsync(params: TransformArgs<TIn, TOut>): EndedFunc<TIn, TOut> {
		if (params.onEnded && params.onEndedAsync) {
			throw new Error("only one of the 'all processed' methods can be asigned.");
		}

		if (params.onEnded) {
			return this.getOnEndedAsAsync(params.onEnded);
		}
		else {
			return params.onEndedAsync || this.finishAsync;
		}
	}

	private getOnEnteredAsAsync(onEntered: OnEnteredFunc<TIn, TOut>): (args: EnteredAsyncArgs<TIn, TOut>) => void {
		return (args: EnteredAsyncArgs<TIn, TOut>) => {
			try {
				const result = onEntered(args);

				if (!result?.then) {
					args.done();
					return;
				}

				result.then(() => { args.done() }, e => { args.failed(e); });
			} catch (error) {
				args.failed(error);
			}
		};
	}

	private getOnEndedAsAsync(onEnded: (args: EndedArgs<TOut>) => void): (args: EndedAsyncArgs<TOut>) => void {
		return (args: EndedAsyncArgs<TOut>) => {
			try {
				onEnded(args);
				args.done();
			} catch (error) {
				args.failed(error);
			}
		};
	}

	private passThroughAsync(args: EnteredAsyncArgs<TIn, TOut>): void {
		args.output.push(<any>args.object);
		args.done();
	}

	private finishAsync(args: EndedAsyncArgs<TOut>): void {
		args.done();
	}
}