import { Output } from "./Output";
import { TransformArgs } from "./TransformArgs";
import { ObjectTransform } from "./ObjectTransform";
import { Writable, Readable, Transform } from "stream";
import { CombinedStream } from "./CombinedStream";

export * from "./EnteredArgs";
export * from "./EndedArgs";

export {
	Output,
	TransformArgs,
	ObjectTransform,
	Writable, Readable, Transform
};

export default class Default {

	public static transform<TIn, TOut>(args: TransformArgs<TIn, TOut>): Transform {
		return new ObjectTransform<TIn, TOut>(args);
	}

	public static combineStreams(...streams: NodeJS.ReadWriteStream[]): NodeJS.ReadWriteStream {
		return new CombinedStream(streams);
	}

	public static combineStreamsList(streams: NodeJS.ReadWriteStream[]): NodeJS.ReadWriteStream {
		return new CombinedStream(streams);
	}

	public static fromArray<T>(array: T[]): Readable {

		let i = 0;
		let stream = new ReadableObjectsStream(() => {
			if (i == array.length) {
				return null;
			}

			return array[i++];
		});

		return stream;
	}
}

class ReadableObjectsStream<TOut> extends Readable {

	constructor(private readObject: () => TOut) {
		super({ objectMode: true });
	}

	public _read(): void {
		let shouldPushMore: boolean = true;

		while (shouldPushMore) {
			shouldPushMore = this.push(this.readObject());
		}
	}
}