import { EnteredArgs, EnteredAsyncArgs } from "./EnteredArgs";
import { EndedArgs, EndedAsyncArgs } from "./EndedArgs";
import { SourceStreamErrorArgs } from "./SourceStreamErrorArgs";

export type OnEnteredFunc<TIn, TOut> = (args: EnteredArgs<TIn, TOut>) => void | Promise<void>;

export interface TransformArgs<TIn, TOut> {
	onEntered?: OnEnteredFunc<TIn, TOut>;
	onEnteredAsync?: (args: EnteredAsyncArgs<TIn, TOut>) => void;
	onEnded?: (args: EndedArgs<TOut>) => void;
	onEndedAsync?: (args: EndedAsyncArgs<TOut>) => void;
	onSourceStreamError?: (args: SourceStreamErrorArgs) => void;
}
