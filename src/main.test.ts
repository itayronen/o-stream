import ObjectStream, { EnteredArgs, EnteredAsyncArgs, EndedArgs, EndedAsyncArgs } from "./main"
import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";

export default function (suite: TestSuite): void {

	suite.describe("transform", suite => {

		suite.describe("Error handling", suite => {
			suite.describe("When an error is thrown from a source stream", suite => {
				suite.test("When default error handling, then the error is propagated.", t => {
					let stream1 = ObjectStream.transform<number, number>({});
					let stream2 = ObjectStream.transform<number, number>({});

					stream1.pipe(stream2);

					let expectedError = "a";
					let actualError: any;

					stream2.on("error", e => actualError = e);

					stream1.emit("error", expectedError);
					expect(actualError).to.equal(expectedError);
				});

				suite.test("When has custom error handler, then handler is invoked.", t => {
					let expectedError = "ab";
					let actualError: any;

					let stream1 = ObjectStream.transform<number, number>({});
					let stream2 = ObjectStream.transform<number, number>({
						onSourceStreamError: args => args.emitError(args.error + "b")
					});

					stream1.pipe(stream2);

					stream2.on("error", e => actualError = e);

					stream1.emit("error", "a");
					expect(actualError).to.equal(expectedError);
				});
			});
		});

		suite.test("When writing a number, it enters as the data.", t => {

			let actual: number = 0;

			let stream = ObjectStream.transform<number, number>({
				onEntered: (args: EnteredArgs<number, number>) => {
					actual = args.object;
				}
			});

			stream.write(4);
			stream.end();

			expect(actual).to.equal(4);
		});

		suite.test("When onEnter is async, then awaits result.", async t => {
			let stream = ObjectStream.transform<number, number>({
				onEntered: async (args: EnteredArgs<number, number>) => {
					await new Promise((resolve) => setTimeout(resolve, 1)).then(() => args.output.push(args.object));
				}
			});

			stream.write(4);
			stream.end();

			for await (const actual of stream)
				expect(actual).to.equal(4);
		});

		suite.test("When onEnter is async, then catches error.", async t => {
			let expectedError = "some error";

			let stream = ObjectStream.transform<number, number>({
				onEntered: async (args: EnteredArgs<number, number>) => {
					await new Promise((_, reject) => setTimeout(() => reject(expectedError), 1));
				}
			});

			stream.write(4);
			stream.end();

			let actualError: any;
			stream.on("error", e => actualError = e);

			for (let i = 0; i < 3; i++)
				await new Promise((resolve) => setTimeout(resolve, 1));

			expect(actualError).to.equal(expectedError);
		});

		suite.test("When writing a number, and transforming it, then output the transformed data.", t => {

			let increamentStream = ObjectStream.transform<number, number>({
				onEntered: (args: EnteredArgs<number, number>) => {
					args.output.push(args.object + 1);
				}
			});

			let num = 4;
			let expected = num + 1;

			increamentStream.write(num);

			let actual = increamentStream.read();

			expect(actual).to.equal(expected);
		});

		suite.test("When writing data to output on 'ended', then data can be consumed.", t => {

			let sum = 0;
			let sumStream = ObjectStream.transform<number, number>({
				onEntered: (args: EnteredArgs<number, number>) => {
					sum += args.object;
				},
				onEnded: (args: EndedArgs<number>) => {
					args.output.push(sum);
				}
			});

			let num1 = 4;
			let num2 = 6;
			let num3 = -1;
			let expected = num1 + num2 + num3;

			sumStream.write(num1);
			sumStream.write(num2);
			sumStream.write(num3);
			sumStream.end();

			let actual = sumStream.read();

			expect(actual).to.equal(expected);
		});

		suite.test("When onEnterAsync and onEndedAsync, then working.", t => {

			let sum = 0;
			let sumStream = ObjectStream.transform<number, number>({
				onEnteredAsync: (args: EnteredAsyncArgs<number, number>) => {
					sum += args.object;
					args.done();
				},
				onEndedAsync: (args: EndedAsyncArgs<number>) => {
					args.output.push(sum);
					args.done();
				}
			});

			let num1 = 4;
			let num2 = 6;
			let num3 = -1;
			let expected = num1 + num2 + num3;

			sumStream.write(num1);
			sumStream.write(num2);
			sumStream.write(num3);
			sumStream.end();

			let actual = sumStream.read();

			expect(actual).to.equal(expected);
		});

		suite.test("When piping, data is transfered.", async t => {
			await new Promise<void>(resolve => {
				let arr = [1, 9, 7];
				let i = 0;

				let stream = ObjectStream.fromArray(arr)
					.pipe(ObjectStream.transform<number, number>({
						onEntered: (args: EnteredArgs<number, number>) => {
							args.output.push(args.object + 1);
						}
					}))
					.pipe(ObjectStream.transform({
						onEntered: (args: EnteredArgs<number, number>) => {
							expect(args.object).to.equal(arr[i++] + 1);
						},
						onEnded: (args: EndedArgs<number>) => {
							expect(i).to.equal(arr.length);
							resolve();
						}
					}));
			});
		});

	});

	suite.describe("fromArray", suite => {

		suite.test("Numbers array.", t => {

			let arr = [1, 9, 7];

			let stream = ObjectStream.fromArray(arr);

			expect(stream.read()).to.equal(arr[0]);
			expect(stream.read()).to.equal(arr[1]);
			expect(stream.read()).to.equal(arr[2]);
			expect(stream.read()).to.equal(null);
		});

	});
}