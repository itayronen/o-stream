import {Output} from "./Output";

export interface EndedArgs<TOut> {
    output: Output<TOut>;
}

export interface EndedAsyncArgs<TOut> extends EndedArgs<TOut> {
    done(): void;
    failed(error: Error): void;
}

export class EndedAsyncArgs<TOut>{

    constructor(public output: Output<TOut>, private callback: (error?: any) => void) {
    }

    public done(): void {
        this.callback();
    }

    public failed(error: any): void {
        this.callback(error);
    }
}
