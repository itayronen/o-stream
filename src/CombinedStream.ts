import { EventEmitter } from "events";

export class CombinedStream extends EventEmitter implements NodeJS.ReadWriteStream {
	private first: NodeJS.ReadWriteStream;
	private last: NodeJS.ReadWriteStream;

	constructor(private streams: NodeJS.ReadWriteStream[]) {
		super();

		if (this.streams.length < 2)
			throw new Error(`At least 2 streams expected. Recieved: ${this.streams.length}`);

		this.first = this.streams[0];
		this.last = this.streams[this.streams.length - 1];

		this.pipeStreams();

		this.propegateFirstAndLastEvents();
	}

	private pipeStreams() {
		for (let i = 0; i < this.streams.length - 1; i++) {
			this.streams[i].pipe(this.streams[i + 1]);
		}
	}

	private propegateFirstAndLastEvents() {
		if (this.first.writable) {
			this.propegateEvents(this.first, "close", "drain", "error", "finish", "pipe", "unpipe");
		}

		if (this.last.readable) {
			this.propegateEvents(this.last, "close", "data", "end", "error", "readable");
		}
	}

	private propegateEvents(emitter: NodeJS.EventEmitter, ...events: string[]) {
		for (let event of events) {
			emitter.on(event, () => {
				this.emit(event, ...arguments);
			});
		}
	}

	// Readable - start

	public get readable(): boolean { return this.last.readable; }
	public read(size?: number): string | Buffer { return this.last.read(size); }
	public setEncoding(encoding: BufferEncoding): this { encoding && this.last.setEncoding(encoding); return this; }
	public pause(): this { this.last.pause(); return this; }
	public resume(): this { this.last.resume(); return this; }
	public isPaused(): boolean { return this.last.isPaused(); }

	public pipe<T extends NodeJS.WritableStream>(destination: T, options?: { end?: boolean; }): T {
		return this.last.pipe(destination, options);
	}

	public unpipe<T extends NodeJS.WritableStream>(destination?: T): this {
		this.last.unpipe(destination);
		return this;
	}

	public unshift(chunk: string | Buffer): void { this.last.unshift(chunk as any); }
	public wrap(oldStream: NodeJS.ReadableStream): this { throw "Unsupported."; }

	public [Symbol.asyncIterator](): AsyncIterableIterator<string | Buffer> { return this.last[Symbol.asyncIterator](); }

	// Readable - end

	// Writable

	public get writable(): boolean { return this.first.writable; }
	public write(buffer: Uint8Array | string, cb?: (err?: Error | null) => void): boolean;
	public write(str: string, encoding?: BufferEncoding, cb?: (err?: Error | null) => void): boolean;
	public write(...args: [any, any, any?]): boolean {
		return this.first.write(...args);
	}
	public end(cb?: () => void): void;
	public end(data: string | Uint8Array, cb?: () => void): void;
	public end(str: string, encoding?: BufferEncoding, cb?: () => void): void;
	public end(...args: [any, any?, any?]): void {
		return this.first.end(...args);
	}

	// Writable - end
}