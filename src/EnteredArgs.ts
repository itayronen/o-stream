import {Output} from "./Output";

export interface EnteredArgs<TIn, TOut> {
    object: TIn;
    output: Output<TOut>
}

export interface EnteredAsyncArgs<TIn, TOut> extends EnteredArgs<TIn, TOut> {
    done(): void;
    failed(error: any): void;
}

export class EnteredAsyncArgs<TIn, TOut> implements EnteredAsyncArgs<TIn, TOut> {

    constructor(public object: TIn, public output: Output<TOut>, private callback: (error?: any) => void) {
    }

    public done(): void {
        this.callback();
    }

    public failed(error: any): void {
        this.callback(error);
    }
}
