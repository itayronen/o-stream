import { CombinedStream } from "./CombinedStream"
import ObjectStream from "./main";
import { TestSuite } from "just-test-api";
import { expect } from "chai";
import { EnteredArgs } from "./EnteredArgs";

export default function (suite: TestSuite): void {
	suite.describe(CombinedStream.name, suite => {
		suite.describe("Ctor", suite => {

			suite.test("Given less than 2 streams, throws.", t => {

				expect(() => new CombinedStream([])).to.throw();
			});

		});

		suite.test("Combine few streams.", t => {
			function createAppendStream(append: string): NodeJS.ReadWriteStream {
				return ObjectStream.transform<string, string>({
					onEntered: (args: EnteredArgs<string, string>) => { args.output.push(args.object + append); }
				});
			}

			let combined = new CombinedStream([
				createAppendStream("b"),
				createAppendStream("c"),
				createAppendStream("d"),
				createAppendStream("e")
			]);

			combined.write("a");

			let actual = combined.read();

			expect(actual).to.equal("abcde");
		});
	});
}
